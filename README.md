# Security Policy Project for gitlab-org

This project is used to manage [security policies](https://docs.gitlab.com/ee/user/application_security/policies/#security-policy-project) for the `gitlab-org` group.

Issues and Merge Requests are visible only to project members due to potentially sensitive discussions about our internal security policies, but the policy configurations themselves are publicly available.